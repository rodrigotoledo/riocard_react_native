/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import 'config/ReactotronConfig';

import { AsyncStorage } from 'react-native';
import createNavigator from "routes";

export default class App extends Component {
  state = {
    userChecked: false,
    userLogged: false,
  }

  async componentDidMount() {
    const email = await AsyncStorage.getItem('@ClientKey:email');

    this.appLoaded(email);
  }

  appLoaded = (email) => {
    this.setState({ userChecked: true, userLogged: !!email });
  }

  render() {
    if (!this.state.userChecked) return null;

    const Routes = createNavigator(this.state.userLogged);

    return <Routes />;
  };
};
