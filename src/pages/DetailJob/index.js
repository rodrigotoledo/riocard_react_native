/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import { View, ActivityIndicator } from "react-native";

import DetailJobBox from "components/DetailJobBox";

import Icon from "react-native-vector-icons/FontAwesome";

import { colors } from "styles";
import styles from "./styles";

export default class DetailJob extends Component {
  static navigationOptions = {
    title: "Realização da OS",
    headerTitleStyle: {
      textAlign: "center",
      alignSelf: "center",
      flex: 1,
      color: colors.primary
    },
    tabBarIcon: () => (
      <Icon name="hourglass-half" size={20} color={colors.lighter} />
    )
  };

  state = {
    loading: false
  };

  render() {
    return (
      <View style={styles.container}>
        {this.state.loading ? (
          <ActivityIndicator style={styles.loading} />
        ) : (
          <DetailJobBox job={this.state.job} />
        )}
      </View>
    );
  }
}
