import React, { Component } from 'react';
import ImagePicker from 'react-native-image-picker';
import { View, Text, TouchableOpacity, ActivityIndicator, ScrollView, TextInput, AsyncStorage, Alert, Image } from "react-native";
import { NavigationActions, withNavigation } from "react-navigation";
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
import "moment/locale/pt-br";
import api from 'services/api';
import styles from './styles';
import SignatureView from './SignatureView';

class CloseJobBox extends Component {

  constructor(props) {
    super(props);
    this.pressed = false;
  }

  state = {
    loading: false,
    photo1: null,
    photo2: null,
    data: null,
    dataString: null,
    motivo_atendimento: '',
    tecnicos_adicionais: '',
    nome_fechou: '',
    email_ou_telefone: '',
  };

  selectPhoto1Tapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      const source = { uri: response };

      // You can also display the image using data:
      // let source = { uri: 'data:image/jpeg;base64,' + response.data };

      this.setState({
        photo1: source
      });
    });
  }

  selectPhoto2Tapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      const source = { uri: response };

      // You can also display the image using data:
      // let source = { uri: 'data:image/jpeg;base64,' + response.data };

      this.setState({
        photo2: source
      });
    });
  }

  saveJob = async () => {
    if (!this.pressed){
      this.setState({ loading: true });
      this.pressed = true;
      try {
        const auth_token = await AsyncStorage.getItem('@ClientKey:auth_token');
        const formdata = new FormData();


        const document = {
          email_ou_telefone: this.state.email_ou_telefone,
          motivo_atendimento: this.state.motivo_atendimento,
          tecnicos_adicionais: this.state.tecnicos_adicionais,
          nome_fechou: this.state.nome_fechou,
        }

        let occurred_at_pars = this.state.occurred_at.split(' ');
        let occurred_at = occurred_at_pars[0].split('/').reverse().join('-') +' '+  occurred_at_pars[1];
        occurred_at = moment(occurred_at).add(3,'hours');

        formdata.append('auth_token', auth_token);
        formdata.append('activity[job_id]', this.state.job.id);
        formdata.append('activity[close_job]', true);
        formdata.append('activity[solved_job]', false);
        formdata.append('activity[occurred_at]', occurred_at.format('YYYY-MM-DD HH:mm'));
        formdata.append('activity[document]', JSON.stringify(document));
        formdata.append('activity[photo1]', JSON.stringify(this.state.photo1));
        formdata.append('activity[photo2]', JSON.stringify(this.state.photo2));
        formdata.append('activity[signature]', this.state.dataString);
        this.setState({ loading: false });
        try {
          const config = { headers: { 'content-type': 'multipart/form-data' } };
          const response = await api.post('/jobs/update', formdata, config);
          Alert.alert('Dados foram enviados para o sistema');
          const resetAction = NavigationActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({ routeName: 'HomeJobs'}),
            ],
          });
          this.props.navigation.dispatch(resetAction);
        } catch (error) {
          console.tron.log(error);
          this.pressed = false;
          this.setState({loading: false})
          Alert.alert('Verifique todos os campos, existem alguns erros.');
        }
      } catch (error) {
        console.tron.log(error);
        this.pressed = false
        this.setState({
          loading: false,
          errorMessage: 'Falha na operacao',
        });
      }
    }
  }

  _showSignatureView() {
    this._signatureView.show(true);
  }

  _onSave(result) {
    const base64String = `data:image/png;base64,${result.encoded}`;
    const dataString = result.encoded;
    this.setState({data: base64String});
    this.setState({dataString: dataString});

    this._signatureView.show(false);
  }


  _onSaveEvent(result) {
      // console.tron.log(result.pathName); /// - for the base64 encoded png
      this.setState()
      // signature = result.pathName; /// - for the base64 encoded png
      // console.tron.log(this.signature);
      //result.pathName - for the file path name
  }
  _onDragEvent() {
        // This callback will be called when the user enters signature
  }

  componentWillMount = () => {
    let job = this.props.navigation.state.params.job;
    this.setState({ job });
  };

  render() {
    const {data} = this.state;
    return (
    <ScrollView>
      <View style={styles.jobContainer}>
        <View style={styles.row}>
          <View style={styles.rowSpecialAttention}>
            <Text style={styles.title}>OS #{this.state.job.id}</Text>
            <Text style={styles.title}>
              Situação: {this.state.job.status_name}
            </Text>
            <Text style={styles.subTitle}>Descrição: {this.state.job.description}</Text>
            <Text style={styles.subTitle}>Observação: {this.state.job.emitter_comments}</Text>
          </View>
          <View>
            <View style={styles.addressContainer}>
              <Text style={[styles.address, styles.subTitle, styles.underline]}>Ponto: {this.state.job.customer.name}</Text>
              <Text style={[styles.address, styles.subTitle]}>
                {JSON.parse(this.state.job.request_document).endereco},{" "}
                {JSON.parse(this.state.job.request_document).bairro} -{" "}
                {JSON.parse(this.state.job.request_document).municipio} /{" "}
                {JSON.parse(this.state.job.request_document).estado}
              </Text>
            </View>
          </View>
        </View>
      </View>

      <View style={[styles.jobContainer, styles.jobContainerWithPadding]}>
        <View style={styles.row}>
          <Text style={[styles.blockTitle, styles.title]}>Motivo</Text>
          <TextInput value={this.state.tecnicos_adicionais} placeholder="Técnico adicionais" onChangeText={(tecnicos_adicionais) => {this.setState({tecnicos_adicionais: tecnicos_adicionais})}} />
          <TextInput value={this.state.nome_fechou} placeholder="Fechado por" onChangeText={(nome_fechou) => {this.setState({nome_fechou: nome_fechou})}} />
          <TextInput value={this.state.motivo_atendimento} placeholder="Motivo do fechamento" onChangeText={(motivo_atendimento) => {this.setState({motivo_atendimento: motivo_atendimento})}} />
          <TextInput value={this.state.email_ou_telefone} placeholder="Modo de solução (email, telefone...)" onChangeText={(email_ou_telefone) => {this.setState({email_ou_telefone: email_ou_telefone})}} />
        </View>
      </View>

      <View style={[styles.jobContainer, styles.jobContainerWithPadding]}>
        <View style={styles.row}>
          <Text style={[styles.blockTitle, styles.title]}>Fotos a serem apresentadas</Text>
          <View style={{flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center', flex: 1}}>
            <TouchableOpacity onPress={this.selectPhoto1Tapped.bind(this)}>
              <View style={[styles.avatar, styles.avatarContainer, {marginBottom: 20}]}>
              { this.state.photo1 === null ? <Text>Primeira foto</Text> :
                <Image style={styles.avatar} source={this.state.photo1.uri} />
              }
              </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={this.selectPhoto2Tapped.bind(this)}>
              <View style={[styles.avatar, styles.avatarContainer, {marginBottom: 20}]}>
              { this.state.photo2 === null ? <Text>Segunda foto</Text> :
                <Image style={styles.avatar} source={this.state.photo2.uri} />
              }
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>

      <View style={[styles.jobContainer, styles.jobContainerWithPadding]}>
        <View style={styles.row}>
          <Text style={[styles.blockTitle, styles.title]}>Data e Hora da realização</Text>
          <View style={styles.calendarContainer}>
            <DatePicker
              style={{width: 200}}
              date={this.state.occurred_at}
              mode="datetime"
              placeholder="Informe a Data"
              format="DD/MM/YYYY HH:mm"
              minDate="2016-05-01"
              confirmBtnText="Confirmar"
              cancelBtnText="Cancelar"
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  left: 0,
                  top: 4,
                  marginLeft: 0
                },
                dateInput: {
                  marginLeft: 36
                }
                // ... You can check the source to find the other keys.
              }}
              onDateChange={(occurred_at) => {this.setState({occurred_at: occurred_at})}}
            />
          </View>
        </View>
      </View>

      <View style={[styles.jobContainer, styles.jobContainerWithPadding]}>
        <View style={styles.row}>
          <Text style={[styles.blockTitle, styles.title]}>Para finalizar o processo é necessário a assinatura do cliente</Text>
          <View style={{ flex: 1, flexDirection: "column", alignItems: 'center' }}>
            <View style={{ flex: 1, flexDirection: "row", alignItems: 'center' }}>
                <TouchableOpacity onPress={this._showSignatureView.bind(this)}>
                  <View style={[styles.flexCenter, {padding: 10}]}>

                    <Text style={styles.signatureText}>
                      {data ? 'Esta é sua assinatura' : 'Clique aqui para assinar.'}
                    </Text>
                    <View style={{paddingBottom: 10}} />
                    {data &&
                      <View style={{backgroundColor: 'white', }}>
                        <Image
                          resizeMode={'contain'}
                          style={{width: 300, height: 300}}
                          source={{uri: data}}
                        />
                      </View>
                    }
                  </View>
                </TouchableOpacity>
                <SignatureView
                  ref={r => this._signatureView = r}
                  rotateClockwise={!!true}
                  onSave={this._onSave.bind(this)}
                />
            </View>
          </View>



          <TouchableOpacity style={[styles.button, styles.buttonDontRealize]} onPress={() => this.saveJob()}>
            {
              this.pressed
                ? <ActivityIndicator size="small"/>
                : <Text style={styles.buttonText}>Finalizar Atendimento</Text>
            }
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>);
  }
};

export default withNavigation(CloseJobBox);
