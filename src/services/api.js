import axios from 'axios';
const api = axios.create({
  // baseURL: "http://192.168.0.101:8080/fe_trans_por/api"
  baseURL: "http://192.168.0.102:8080/fe_trans_por/api"
  // baseURL: "http://192.168.129.48:8080/fe_trans_por/api"
  // baseURL : "http://172.20.10.7:8080/fe_trans_por/api"
  // baseURL: "http://192.168.135.242:8080/fe_trans_por/api"
  // baseURL: "http://192.168.0.136:8080/fe_trans_por/api"
  // baseURL: "http://192.168.0.102:8080/fe_trans_por/api"
  // baseURL : "http://172.20.10.7:8080/fe_trans_por/api"
  // baseURL: "http://192.168.132.128:8080/fe_trans_por/api"
  // baseURL: "http://app.pxos.com.br:8080/fe_trans_por/api"
});

export default api;
